from tkeproject import db, app, bcrypt
from tkeproject.models import Customers, Orders, Products, MpesaTransactions
from flask import Flask

from datetime import datetime, timedelta
from faker import Faker

import random
import string

fake = Faker()


def add_customers():
    for _ in range(100):
        customer = Customers(
            firstname=fake.first_name(),
            lastname=fake.last_name(),
            email=fake.email(),
            phone=fake.msisdn(),
            password=bcrypt.generate_password_hash('123456').decode('utf-8'),
            address=fake.street_address(),
            city=fake.city(),
        )
        db.session.add(customer)
    db.session.commit()

def add_orders():
    customers = Customers.query.all()

    for _ in range(1000):
        #choose a random customer
        customer = random.choice(customers)

        orderdate = fake.date_time_this_year()

        couponcode = random.choices([None, 'HOLIDAY_X', 'OPENNING'], [90, 5, 5])[0]
        orderstatus = random.choices(['Pending', 'Processing', 'Delivered'], [10, 10, 80])[0]

        orderamount = random.randint(30,150)

        order = Orders(
            customerid=customer.id,
            orderdate=orderdate,
            orderstatus=orderstatus,
            couponcode=couponcode,
            orderamount=orderamount
        )

        db.session.add(order)
    db.session.commit()

def add_products():
    for _ in range(100):

        type = random.choices(['Movie', 'Series', None], [59,39,2])[0]
        genre = random.choices(['Action', 'Comedy', 'Cartoon', 'General', 'Horror'], [20,20,20,20,20])[0]
        imagelocation = random.choices(['f1.jpg', 'f2.jpg', 'f3.jpg', 'f4.jpg', 'f5.jpg', 'f6.jpg', 'f7.jpg', 'f8.jpg', 'f9.jpg', 'f10.jpg',
                                        'f11.jpg', 'f12.jpg', 'f13.jpg', 'f14.jpg', 'f15.jpg', 'f16.jpg', 'f17.jpg', 'f18.jpg', 'f19.jpg', 'f20.jpg',
                                        'f21.jpg', 'f22.jpg', 'f23.jpg', 'f24.jpg', 'f25.jpg', 'f26.jpg', 'f27.jpg', 'f28.jpg', 'f29.jpg', 'f30.jpg',
                                        'f31.jpg', 'f32.jpg', 'f33.jpg', 'f34.jpg', 'f35.jpg', 'f36.jpg', 'f37.jpg', 'f38.jpg', 'f39.jpg', 'f40.jpg',
                                        'f41.jpg', 'f42.jpg', 'f43.jpg', 'f44.jpg', 'f45.jpg', 'f46.jpg', 'f47.jpg', 'f48.jpg', 'f49.jpg', 'f50.jpg',
                                        'f61.jpg', 'f62.jpg', 'f63.jpg', 'f64.jpg', 'f65.jpg', 'f66.jpg', 'f67.jpg', 'f68.jpg', 'f69.jpg', 'f60.jpg',
                                        'f51.jpg', 'f52.jpg', 'f53.jpg', 'f54.jpg', 'f55.jpg', 'f56.jpg', 'f57.jpg', 'f58.jpg', 'f59.jpg', 'f70.jpg'])[0]

        product = Products(
            title=fake.company(),
            crew=fake.name(),
            price=random.randint(30,50),
            year=fake.year(),
            imagelocation=imagelocation,
            description=fake.sentence(),
            type=type,
            genre=genre
        )
        db.session.add(product)
    db.session.commit()

def add_order_products():
    orders = Orders.query.all()
    products = Products.query.all()

    for order in orders:
        #select random k
        k = random.randint(1,5)
        # select random products
        purchased_products = random.sample(products, k)
        order.products.extend(purchased_products)

    db.session.commit()


def add_transactions():

    for i in range(100):

        fake_transactionid =''.join(random.choice(string.ascii_uppercase+string.digits) for j in range(10))

        t_type = random.choices(['PayBill', 'BuyGoods'], [95,5])[0]
        invoice = random.randint(1000,2000)

        transaction = MpesaTransactions(
                            transactiontype = t_type,
                            transactionid = fake_transactionid,
                            transactiontime =  '20191122063845',
                            transactionamount = '150',
                            business_shortcode = '600638',
                            msisdn = '254708374149',
                            firstname = 'John',
                            lastname = 'Doe'
                        )
        db.session.add(transaction)
    db.session.commit()

def create_random_data():
    print("creating tables...")
    db.create_all()
    print("adding customers...")
    add_customers()
    print("adding films...")
    add_products()
    print("adding orders...")
    add_orders()
    #add_order_products()
    print("adding transactions...")
    add_transactions()
