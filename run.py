from tkeproject import app

application = app

if __name__ == "__main__":
    app.run(debug=True, port=5016, host='0.0.0.0')
