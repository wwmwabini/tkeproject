from flask import render_template, request, flash, url_for, redirect
from tkeproject import db, app, mail
from tkeproject.models import Products, Cart, HomePage
from tkeproject.forms import AddToCartForm, ContactForm
from tkeproject.shop_routes import cart_count
from flask_login import current_user
from flask_mail import Message
from config import MAIL_DEFAULT_SENDER


def send_contact_form_email(name, email, subject, message):

    msg = Message('Royalty Ent - Contact Us ', sender=('Royalty Ent', MAIL_DEFAULT_SENDER), recipients=[MAIL_DEFAULT_SENDER])

    msg.html = render_template('message_contact_form.html', name=name, email=email, subject=subject, message=message)

    msg.reply_to = email

    mail.send(msg)



#General routes
@app.route('/')
@app.route('/home/')
def home():
    cartcount = cart_count()
    movies = Products.query.filter_by(featured=True, type='Movie').all()
    series = Products.query.filter_by(featured=True, type='Series').all()
    archives = Products.query.filter_by(archived=True).all()
    sliders = HomePage.query.all()


    return render_template('index.html', title="Home", cartcount=cartcount, movies=movies, series=series, archives=archives, sliders=sliders)

@app.route('/movies/', methods=['GET', 'POST'])
def movies():
    cartcount = cart_count()
    page = request.args.get('page', 1, type=int)

    movies = Products.query.filter_by(type='Movie').order_by(Products.id.desc()).paginate(page=page, per_page=15)

    return render_template('movies.html', title="Movies", cartcount=cartcount, movies=movies)

@app.route('/series/', methods=['GET', 'POST'])
def series():
    cartcount = cart_count()

    page = request.args.get('page', 1, type=int)

    series = Products.query.filter_by(type='Series').order_by(Products.id.desc()).paginate(page=page, per_page=15)

    return render_template('series.html', title="Series", cartcount=cartcount, series=series)


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    cartcount = cart_count()

    form = ContactForm()

    if form.validate_on_submit():

        send_contact_form_email(form.name.data, form.email.data, form.subject.data, form.message.data)

        flash('Your message has been sent. We will get back within 24hrs.', 'success')
        return redirect(url_for('contact'))

    return render_template('contact.html', title="Contact Us", cartcount=cartcount, form=form)
