from tkeproject import db, app, login_manager
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from datetime import datetime
from flask_login import UserMixin


@login_manager.user_loader
def loaduser(customer_id):
    return Customers.query.get(int(customer_id))

accesslevels = {
    'customer': 0,
    'admin': 1,
}

#Models
class Customers(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    firstname = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    phone = db.Column(db.String(13), unique=True, nullable=False)
    imagelocation = db.Column(db.String(50), nullable=False, default='defaultprofile.png')
    password = db.Column(db.String(60), nullable=False)
    address = db.Column(db.String(100), nullable=False, default='Makongeni, Thika')
    city = db.Column(db.String(50), nullable=False, default='Thika')
    state = db.Column(db.String(50), nullable=False, default='Kiambu')
    registrationdate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    notificationstatus = db.Column(db.Boolean, nullable=False, default=True)
    notificationstatuschangedate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    accesslevel = db.Column(db.String(50), nullable=False, default=accesslevels['customer'])

    orders = db.relationship('Orders', backref='customers')

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'customerid': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            customerid = s.loads(token)['customerid']
        except:
            return None
        return Customers.query.get(customerid)

"""
class Admins(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    firstname = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    phone = db.Column(db.String(13), unique=True, nullable=False)
    imagelocation = db.Column(db.String(50), nullable=False, default='defaultprofile.png')
    password = db.Column(db.String(60), nullable=False)
    address = db.Column(db.String(100), nullable=False)
    registrationdate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'adminid': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            adminid = s.loads(token)['adminid']
        except:
            return None
        return Admins.query.get(adminid)
"""
#orders_products = db.Table('orders_products',
#    db.Column('orderid', db.Integer, db.ForeignKey('orders.id'), primary_key=True),
#    db.Column('productid', db.Integer, db.ForeignKey('products.id'), primary_key=True)
#)

class orders_products(db.Model):
    orderno = db.Column(db.String(10), db.ForeignKey('orders.orderno'), primary_key=True)
    productid = db.Column(db.Integer, db.ForeignKey('products.id'), primary_key=True)

#cart_products = db.Table('cart_products',
#    db.Column('cartid', db.Integer, db.ForeignKey('cart.id'), primary_key=True),
#    db.Column('productid', db.Integer, db.ForeignKey('products.id'), primary_key=True)
#)

class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    orderno=db.Column(db.String(10), nullable=False, unique=True)
    orderdate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    orderstatus = db.Column(db.Enum('Pending','Processing','Delivered'), nullable=False, server_default="Pending")
    couponcode = db.Column(db.String(100))
    invoicenumber = db.Column(db.Integer, db.ForeignKey('invoices.id'))
    shipping_option = db.Column(db.Integer, default=0)
    customerid = db.Column(db.Integer, db.ForeignKey('customers.id'), nullable=False)
    orderamount = db.Column(db.Integer, default=0)
    amountpaid = db.Column(db.Integer, default=0)
    itemcount = db.Column(db.Integer, default=1)
    #products = db.relationship('Products', secondary=orders_products)

class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(100), nullable=False)
    imagelocation = db.Column(db.String(50), nullable=False, default='defaultfilm.jpg')
    crew = db.Column(db.String(255), nullable=False)
    year = db.Column(db.SmallInteger)
    price = db.Column(db.Integer, nullable=False, default=50)
    description = db.Column(db.String(1000))
    featured = db.Column(db.Boolean, nullable=False, default=False)
    archived = db.Column(db.Boolean, nullable=False, default=False)
    type = db.Column(db.Enum('Movie','Series'), nullable=False, server_default="Movie")
    genre = db.Column(db.Enum('Action','Comedy','Cartoon','Horror','General'), nullable=False, server_default="General")


class Cart(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    quantity = db.Column(db.Integer, nullable=False, default=1)
    customerid = db.Column(db.Integer, db.ForeignKey('customers.id'), nullable=False)
    productid = db.Column(db.Integer, db.ForeignKey('products.id'), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    price= db.Column(db.Integer, nullable=False, default=30)
    imagelocation = db.Column(db.String(50), nullable=False, default='defaultfilm.jpg')

class MpesaTransactions(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    intasend_invoice_id = db.Column(db.String(50), nullable=True)
    intasend_state = db.Column(db.String(50), nullable=False, default='PROCESSING')
    intasend_provider = db.Column(db.String(50), nullable=False, default='M-PESA')
    intasend_charges = db.Column(db.Integer, nullable=True)
    intasend_net_amount = db.Column(db.Integer, nullable=True)
    intasend_currency = db.Column(db.String(50), nullable=False, default='KES')
    intasend_value = db.Column(db.Integer, nullable=True)
    intasend_account = db.Column(db.String(50), nullable=True)
    intasend_api_ref = db.Column(db.String(50), nullable=True)
    intasend_host = db.Column(db.String(100), nullable=True)
    intasend_failed_reason = db.Column(db.String(500), nullable=True)
    intasend_failed_code = db.Column(db.String(50), nullable=True)
    intasend_failed_code_link = db.Column(db.String(500), nullable=True)
    intasend_created_at = db.Column(db.String(100), nullable=False, default=datetime.utcnow)
    intasend_updated_at = db.Column(db.String(100), nullable=False, default=datetime.utcnow)


class Invoices(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    amount = db.Column(db.Integer, nullable=False)
    orderid = db.Column(db.Integer, nullable=False)

class HomePage(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    imagelocation = db.Column(db.String(50), nullable=False, default='defaultfilm.jpg')
    filmtitle = db.Column(db.String(100), nullable=False)
