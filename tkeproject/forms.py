from flask_wtf import FlaskForm, RecaptchaField
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, SelectField, HiddenField, EmailField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from tkeproject.models import  Customers


class ContactForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=50)])
    email = EmailField('Email', validators=[DataRequired()])
    subject = StringField('Subject', validators=[DataRequired(), Length(min=2, max=100)])
    message = TextAreaField('Message', validators=[DataRequired()])
    recaptcha = RecaptchaField()
    submit = SubmitField('Send Message')

class RegisterForm(FlaskForm):
    firstname = StringField('First Name', validators=[DataRequired(), Length(min=2, max=50)])
    lastname = StringField('Last Name', validators=[DataRequired(), Length(min=2, max=50)])
    email = StringField('Email Address', validators=[DataRequired(), Length(min=2, max=100)])
    phone = StringField('Phone Number', validators=[DataRequired(), Length(min=10, max=13)])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators = [DataRequired(), EqualTo('password')])
    address=StringField('Address', validators=[Length(min=2, max=100)])
    city=StringField('City', validators=[Length(min=2, max=50)])
    state=SelectField('State', choices=[('kiambu', 'Kiambu'), ('nairobi', 'Nairobi')])
    notificationstatus = BooleanField('Opt Out of SMS?')
    recaptcha = RecaptchaField()

    def validate_email(self, email):
        result = Customers.query.filter_by(email=email.data).first()
        if result:
            raise ValidationError('Email already registered. Please choose a diffrent email.')

    def validate_phone(self, phone):
        result = Customers.query.filter_by(phone=phone.data).first()
        if result:
            raise ValidationError('Phone already registered. Please choose a diffrent phone number.')

class LoginForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

class PasswordResetForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired()])
    submit = SubmitField('Send Instructions')

    def validate_email(self, email):
        result = Customers.query.filter_by(email=email.data).first()
        if not result:
            raise ValidationError('Email does not exist in our system.')

class NewPasswordForm(FlaskForm):
    password = PasswordField('New Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')


class UpdateDetailsForm(FlaskForm):
    firstname = StringField('First Name', validators=[DataRequired(), Length(min=2, max=50)])
    lastname = StringField('Last Name', validators=[DataRequired(), Length(min=2, max=50)])
    email = StringField('Email Address', validators=[DataRequired(), Length(min=2, max=100)])
    phone = StringField('Phone Number', validators=[DataRequired(), Length(min=10, max=13)])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators = [DataRequired(), EqualTo('password')])
    address=StringField('Address', validators=[Length(min=2, max=100)])
    city=StringField('City', validators=[Length(min=2, max=50)])
    state=SelectField('State', choices=[('kiambu', 'Kiambu'), ('nairobi', 'Nairobi')])
    notificationstatus = BooleanField('Opt Out of SMS?')

    def validate_email(self, email):
        result = Customers.query.filter_by(email=email.data).first()
        if result:
            raise ValidationError('Email already registered. Please choose a diffrent email.')

    def validate_phone(self, phone):
        result = Customers.query.filter_by(phone=phone.data).first()
        if result:
            raise ValidationError('Phone already registered. Please choose a diffrent phone number.')


class AdminLoginForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

class AdminPasswordResetForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired()])
    recaptcha = RecaptchaField()
    submit = SubmitField('Send Instructions')

    def validate_email(self, email):
        result = Admins.query.filter_by(email=email.data).first()
        if not result:
            raise ValidationError('Email does not exist in our system.')

class AdminNewPasswordForm(FlaskForm):
    password = PasswordField('New Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')



class AddToCartForm(FlaskForm):
    productid = HiddenField(validators=[DataRequired()])
    quantity = HiddenField('quantity', default='1',validators=[DataRequired()])

class ClearAllCartForm(FlaskForm):
    btn_identifier = HiddenField(default='clearallcart')
    submit1 = SubmitField('Clear All')

class RemoveSingleItemForm(FlaskForm):
    prod_id = HiddenField()
    btn_identifier = HiddenField(default='removesingleitem')
    submit2 = SubmitField('Remove')

class PayNowForm(FlaskForm):
    mpesacode = StringField("MPESA Transaction Code")
    submit3 = SubmitField('Pay Now')

class OrderNowForm(FlaskForm):
    shipping_option = SelectField('Shipping Option', choices=[('0', 'No Shipping - sh0'), ('50', 'Thika Town - sh50'), ('100', 'Witethia/Juja - sh100'), ('150', 'Ruiru/K-Road - sh1500'), ('200', 'Nairobi CBD - sh200')])
    submit = SubmitField('Order Now')

class UploadFilmForm(FlaskForm):
    title = StringField('Title', validators=[Length(min=1, max=300)])
    mainactors=StringField('Main Actors', validators=[Length(min=1, max=300)])
    year=StringField('Year', validators=[Length(min=4, max=4)])
    description=StringField('Description', validators=[Length(min=1, max=500)])
    type=SelectField('Type', choices=[('Movie', 'Movie'), ('Series', 'Series')])
    genre=SelectField('Genre', choices=[('Action', 'Action'), ('Comedy', 'Comedy'), ('Cartoon', 'Cartoon'), ('Cartoon', 'Cartoon'), ('General', 'General')])
    picture = FileField('Browse an Image', validators=[FileAllowed(['jpg', 'png', 'jpeg', 'gif'])])
    submit = SubmitField('Add New')
