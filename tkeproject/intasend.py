import requests
from flask import request,json, jsonify
from tkeproject import app, db
from tkeproject.models import MpesaTransactions
from requests.auth import HTTPBasicAuth
from config import INTASEND_CHALLENGE


@app.route('/c2b/is/confirmation', methods=['POST'])
def confirmation():

    results = request.get_json(silent=True)

    if results['challenge'] == INTASEND_CHALLENGE:
        transaction = MpesaTransactions(
                    intasend_invoice_id = results['invoice_id'],
                    intasend_state = results['state'],
                    intasend_provider = results['provider'],
                    intasend_charges = results['charges'],
                    intasend_net_amount = results['net_amount'],
                    intasend_currency = results['currency'],
                    intasend_value = results['value'],
                    intasend_account = results['account'],
                    intasend_api_ref =results['api_ref'],
                    intasend_host = results['host'],
                    intasend_failed_reason = results['failed_reason'],
                    intasend_failed_code = results['failed_code'],
                    intasend_failed_code_link = results['failed_code_link'],
                    intasend_created_at = results['created_at'],
                    intasend_updated_at = results['updated_at']
                )
        db.session.add(transaction)
        db.session.commit()

        if results['state'] == 'COMPLETE':
            order = Orders.query.filter_by(orderno=results['api_ref'])
            order.amountpaid = results['value']
            order.orderstatus = 'Processing' #use same logic as accesslevels instead of hardcording in future versions
            db.session.commit()
    else:
        return jsonify(message='Invalid challenge phrase'), 403

    return jsonify(message='Success'), 201
