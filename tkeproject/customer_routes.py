from flask import render_template, redirect, url_for, flash, request
from tkeproject import db, app, bcrypt, mail
from tkeproject.forms import RegisterForm, LoginForm, PasswordResetForm, NewPasswordForm, UpdateDetailsForm
from tkeproject.models import Customers, Orders
from tkeproject.shop_routes import cart_count
from flask_login import login_user, login_required, logout_user, current_user
from flask_mail import Message
from config import MAIL_DEFAULT_SENDER


def send_reset_email(customer):
    token = customer.get_reset_token()

    msg = Message('Password Reset Instructions', sender=('Royalty Ent', MAIL_DEFAULT_SENDER), recipients=[customer.email])

    msg.html = render_template('customers/passwordreset/message.html', token=token)

    mail.send(msg)


#Customer dashboard routes
@app.route('/customer/register', methods=['GET', 'POST'])
def customerregister():

    if current_user.is_authenticated:
        flash('Logout first to be able to perform that action!', 'info')
        return redirect(url_for('home'))

    form = RegisterForm()


    if form.validate_on_submit():
        hashedpassword = bcrypt.generate_password_hash(form.password.data).decode('utf-8')

        customer = Customers(firstname=form.firstname.data, lastname=form.lastname.data,
                            email=form.email.data, phone=form.phone.data, password=hashedpassword,
                            address=form.address.data,
                            city=form.city.data, state=form.state.data)
        db.session.add(customer)
        db.session.commit()

        flash('Account created successfully. You can now login!', 'success')

        return redirect(url_for('customerlogin'))
    return render_template('customers/register.html', title="Register", form=form)


@app.route('/customer/login', methods=['GET', 'POST'])
def customerlogin():
    if current_user.is_authenticated:
        flash('Logout first to perform that action!', 'info')
        return redirect(url_for('home'))

    form = LoginForm()

    if form.validate_on_submit():

        customer = Customers.query.filter_by(email=form.email.data).first()

        if customer and bcrypt.check_password_hash(customer.password, form.password.data):
            login_user(customer, remember=form.remember.data)
            return redirect(url_for('customerdashboard'))
        else:
            flash('Invalid login. Please try again', 'danger')
            return redirect(url_for('customerlogin'))
    return render_template('customers/login.html', title="Login", form=form)


@app.route('/customer/logout')
def customerlogout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/customer/dashboard')
@login_required
def customerdashboard():
    page = request.args.get('page', 1, type=int)
    orders = Orders.query.filter_by(customerid=current_user.id).order_by(Orders.id.desc()).paginate(page=page, per_page=5)
    orderscount = Orders.query.filter_by(customerid=current_user.id).count()

    return render_template('customers/dashboard.html', title="Dashboard", orders=orders, orderscount=orderscount )

@app.route('/customer/profile', methods=['GET', 'POST'])
@login_required
def customerprofile():

    form = UpdateDetailsForm()

    if form.validate_on_submit():

        current_user.firstname = form.firstname.data
        current_user.lastname = form.lastname.data
        current_user.email = form.email.data
        current_user.phone = form.phone.data
        current_user.notificationstatus = form.notificationstatus.data

        db.session.commit()

        flash('Your details have been updated successfully.', 'success')

        return redirect(url_for('customerprofile'))


    elif request.method == 'GET':
        form.firstname.data = current_user.firstname
        form.lastname.data = current_user.lastname
        form.email.data = current_user.email
        form.phone.data = current_user.phone
        form.notificationstatus.data = current_user.notificationstatus

    return render_template('customers/profile.html', title="Profile", form=form)


@app.route('/customer/passwordreset', methods=['GET', 'POST'])
def customerpasswordreset():

    if current_user.is_authenticated:
        flash('Logout first to perform that action!', 'info')
        return redirect(url_for('home'))

    form = PasswordResetForm()

    if form.validate_on_submit():

        customer = Customers.query.filter_by(email = form.email.data).first()
        send_reset_email(customer)
        return redirect(url_for('customercheckmail'))

    return render_template('customers/passwordreset/passwordreset.html', title="Password Reset", form=form)


@app.route('/customer/checkmail')
def customercheckmail():
    if current_user.is_authenticated:
        flash('Logout first to be able to perform that action!', 'info')
        return redirect(url_for('home'))

    return render_template('customers/passwordreset/checkemail.html', title="Password Reset")

@app.route('/customer/newpassword/<token>', methods=['GET', 'POST'])
def customernewpassword(token):

    if current_user.is_authenticated:
        flash('Logout first to be able to perform that action!', 'info')
        return redirect(url_for('home'))

    customer = Customers.verify_reset_token(token)
    if not customer:
        flash('Token is expired or invalid', 'warning')
        return redirect(url_for('customerpasswordreset'))

    form = NewPasswordForm()

    if form.validate_on_submit():
        hashedpassword = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        customer.password = hashedpassword
        db.session.commit()

        flash('Password updated successfully.', 'success')
        return redirect(url_for('customerlogin'))

    return render_template('customers/passwordreset/newpassword.html', title="Password Reset", form=form)
