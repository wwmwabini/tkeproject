from flask import render_template, redirect, flash, url_for, request
from tkeproject import app

@app.errorhandler(401)
def unauthorized_error(error):
    flash('Please create an account or login first.', 'info')
    return redirect(url_for('customerlogin')), 401

@app.errorhandler(403)
def forbidden_error(error):
    flash('Permission denied. You are not allowed to access that resource.', 'warning')
    return render_template('errors/403.html'), 403

@app.errorhandler(404)
def pagenotfound_error(error):
    flash('Page not found. The resource cannot be found.', 'info')
    return render_template('errors/404.html'), 404

@app.errorhandler(500)
def internalsever_error(error):
    flash('Internal Server Error', 'info')
    return render_template('errors/500.html'), 500

@app.errorhandler(503)
def serviceunavailable_error(error):
    flash('Service Temporarily Unavailable', 'info')
    return render_template('errors/503.html'), 503
