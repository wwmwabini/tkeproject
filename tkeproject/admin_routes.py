import requests
from PIL import Image
from flask import render_template, flash, redirect, url_for, request
from datetime import datetime, timedelta
from tkeproject import db, app, bcrypt, mail
from tkeproject.forms import LoginForm, PasswordResetForm, NewPasswordForm, UploadFilmForm
from tkeproject.models import Customers, accesslevels, Orders, Products
from tkeproject.shop_routes import cart_count
from flask_login import login_user, login_required, logout_user, current_user
from flask_mail import Message
from config import MAIL_DEFAULT_SENDER, JISORT_SMS_USERNAME, JISORT_SMS_PASSWORD

def send_reset_email(user):
    token = user.get_reset_token()

    msg = Message('Password Reset Instructions', sender=('Royalty Ent', MAIL_DEFAULT_SENDER), recipients=[user.email])

    msg.html = render_template('admin/passwordreset/message.html', token=token)

    mail.send(msg)

def send_sms(phone, message):
    print(phone)
    print(message)
    endpoint = "https://my.jisort.com/messenger/send_message/"+"?username="+JISORT_SMS_USERNAME+"&password="+JISORT_SMS_PASSWORD+"&recipients="+phone+"&message="+message
    payload = {}
    headers = {}
    response = requests.request("GET", endpoint, headers=headers, data=payload)

    return 0

def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/images/films', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


#Admin dashboard routes
@app.route('/admin/login', methods=['GET', 'POST'])
def adminlogin():

    if current_user.is_authenticated:
        flash('Logout first to perform that action', 'info')
        return redirect(url_for('admindashboard'))

    form = LoginForm()

    if form.validate_on_submit():

        user = Customers.query.filter_by(email=form.email.data, accesslevel=accesslevels['admin']).first()

        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            return redirect(url_for('admindashboard'))
        else:
            flash('Invalid login. Please try again', 'danger')
            return redirect(url_for('adminlogin'))

    return render_template('admin/login.html', title="Admin Login", form=form)


@app.route('/admin/passwordreset', methods=['GET', 'POST'])
def adminpasswordreset():

    if current_user.is_authenticated:
        flash('Logout first to perform that action')
        return redirect(url_for('home'))

    form = PasswordResetForm()

    if form.validate_on_submit():
        user = Customers.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        return redirect(url_for('admincheckmail'))

    return render_template('admin/passwordreset/passwordreset.html', title="Admin Password Reset", form=form)


@app.route('/admin/checkmail')
def admincheckmail():
    if current_user.is_authenticated:
        flash('Logout first to be able to perform that action!', 'info')
        return redirect(url_for('home'))

    return render_template('admin/passwordreset/checkemail.html', title="Password Reset")


@app.route('/admin/newpassword/<token>', methods=['GET', 'POST'])
def adminnewpassword(token):

    if current_user.is_authenticated:
        flash('Logout first to be able to perform that action!', 'info')
        return redirect(url_for('home'))

    user = Customers.verify_reset_token(token)
    if not admin:
        flash('Token is expired or invalid', 'warning')
        return redirect(url_for('adminpasswordreset'))

    form = NewPasswordForm()

    if form.validate_on_submit():
        hashedpassword = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        admin.password = hashedpassword
        db.session.commit()

        flash('Password updated successfully.', 'success')
        return redirect(url_for('adminlogin'))

    return render_template('admin/passwordreset/newpassword.html', title="Password Reset", form=form)



@app.route('/admin/dashboard')
@login_required
def admindashboard():
    '''show incomes, number of customers, manage customers - send SMSes, add, remove customers or opt them out of SMS'''
    cartcount = cart_count()

    user = Customers.query.filter_by(id=current_user.id, accesslevel=accesslevels['admin']).first()
    usercount = Customers.query.count()
    ordercount = Orders.query.count()
    filmcount = Products.query.count()
    salescount = db.session.query(db.func.sum(Orders.orderamount)).scalar()

    adminpagecards = [
    {
        'color': 'danger',
        'value': usercount,
        'title': 'Users'
    },
    {
        'color': 'success',
        'value': ordercount,
        'title': 'Orders'
    },
    {
        'color': 'warning',
        'value': salescount,
        'title': 'Sales'
    },
    {
        'color': 'info',
        'value': filmcount,
        'title': 'Films'
    }
    ]


    if not user:
        flash('You do not have access to this resource.', 'warning')
        return redirect(request.referrer)
    else:
        orders = Orders.query.order_by(Orders.id.desc()).limit(5).all()

        dailysales = db.session.query(db.func.sum(Orders.orderamount)).filter(Orders.orderdate > (datetime.now() - timedelta(days=1))).scalar()
        last30sales = db.session.query(db.func.sum(Orders.orderamount)).filter(Orders.orderdate > (datetime.now() - timedelta(days=30))).scalar()
        last365sales = db.session.query(db.func.sum(Orders.orderamount)).filter(Orders.orderdate > (datetime.now() - timedelta(days=365))).scalar()

    return render_template('admin/dashboard.html', title="Dashboard", cartcount=cartcount,
                            orders=orders, adminpagecards=adminpagecards,
                            dailysales=dailysales, last30sales=last30sales,
                            last365sales=last365sales
                            )


@app.route('/admin/view/allorders', methods=['GET', 'POST'])
@login_required
def adminviewallorders():
    '''view orders and statuses, fulfil orders'''
    cartcount = cart_count()
    page = request.args.get('page', 1, type=int)
    orders = Orders.query.order_by(Orders.id.desc()).paginate(page=page, per_page=10)

    if request.method == 'POST':

        order = Orders.query.filter_by(id=request.form['orderid']).first()
        order.orderstatus = request.form['orderstatus']
        db.session.commit()

        flash('Order accepted', 'success')
        return redirect(url_for('adminviewallorders'))

    return render_template('admin/orders/orders.html', title="All Orders", cartcount=cartcount, orders=orders)



@app.route('/admin/view/order', methods=['GET', 'POST'])
@login_required
def adminvieworder():
    '''view specific details about an order'''
    order = Orders.query.filter_by(id=request.form['orderid']).first()

    return render_template('admin/orders/orderdetails.html', title="Order Details", cartcount=cartcount, order=order)




@app.route('/admin/view/allfilms', methods=['GET', 'POST'])
@login_required
def adminviewallfilms():
    cartcount = cart_count()

    page = request.args.get('page', 1, type=int)
    films = Products.query.order_by(Products.id.desc()).paginate(page=page, per_page=6)
    filmcount = Products.query.count()

    form = UploadFilmForm()

    if form.validate_on_submit():
        if form.picture.data:
            picturefile = save_picture(form.picture.data)

        if form.type.data == 'Movie':
            filmtype='Movie'
        else:
            filmtype="Series"

        newfilm = Products(
            title=form.title.data,
            crew=form.mainactors.data,
            year=form.year.data,
            description=form.description.data,
            type=filmtype,
            genre=form.genre.data
        )
        db.session.add(newfilm)
        db.session.commit()

        flash('Film has been added!', 'success')
        return redirect(url_for('adminviewallfilms'))

    return render_template('admin/films/films.html', title="Movies and Series", films=films, filmcount=filmcount, form=form)


@app.route('/admin/view/film', methods=['GET', 'POST'])
@login_required
def adminviewfilm():
    cartcount = cart_count()

    film = Products.query.filter_by(id=request.form['productid']).first()

    return render_template('admin/films/filmdetails.html', title="Movies and Series", film=film)



@app.route('/admin/view/allcustomers', methods=['GET', 'POST'])
@login_required
def adminviewallcustomers():
    '''view orders and statuses, fulfil orders'''
    cartcount = cart_count()

    page = request.args.get('page', 1, type=int)

    customers = Customers.query.order_by(Customers.id.desc()).paginate(page=page, per_page=7)

    return render_template('admin/customers/customers.html', title="All Customers", cartcount=cartcount, customers=customers)


@app.route('/admin/view/customer', methods=['GET', 'POST'])
@login_required
def adminviewcustomer():
    '''view specific details about an order'''

    return render_template('admin/customers/customerdetails.html', title="Customer Details", cartcount=cartcount)


@app.route('/admin/sms', methods=['GET', 'POST'])
@login_required
def sms():

    subscribed = Customers.query.filter_by(notificationstatus=1).count()
    unsubscribed = Customers.query.filter_by(notificationstatus=0).count()
    sentmessagequantity = 0
    sentcampaignsquantity = 0

    latestactivity = Customers.query.order_by(Customers.notificationstatuschangedate.desc()).limit(5).all()


    subscriptions = [
    {
        'color': 'success',
        'value': subscribed,
        'title': 'Subscribed Users'
    },
    {
        'color': 'danger',
        'value': unsubscribed,
        'title': 'Unsubscribed Users'
    },
    {
        'color': 'info',
        'value': sentmessagequantity,
        'title': 'Messages Sent'
    },
    {
        'color': 'warning',
        'value': sentcampaignsquantity,
        'title': 'Campaigns Sents'
    }
    ]

    if request.method == 'POST':
        if request.form['subscribed'] == 'subscribed_users':
            message = request.form['message']

            users = Customers.query.filter_by(notificationstatus=1).all()

            for user in users:
                send_sms(user.phone, message)
            flash('Messages queued for sending to subscribed customers', 'info')
            return redirect(url_for('sms'))

        elif request.form['subscribed'] == 'all_users':
            message = request.form['message']

            users = Customers.query.all()

            for user in users:
                send_sms(user.phone, message)
            flash('Messages queued for sending to all customers', 'info')
            return redirect(url_for('sms'))

    return render_template('admin/sms/managesms.html', title="SMS", subscriptions=subscriptions, latestactivity=latestactivity)
