from flask import render_template, request, redirect, flash, url_for, jsonify
from tkeproject import db, app
from flask_login import login_user, login_required, logout_user, current_user
from tkeproject.models import Cart, Products, MpesaTransactions, Orders, orders_products
from tkeproject.forms import ClearAllCartForm, RemoveSingleItemForm, PayNowForm, OrderNowForm
from config import INTASEND_SUCCESS_URL,INTASEND_PUBLISHABLE_KEY, INTASEND_LIVENESS

import json
import random
import string

#Functions
def cart_count():
    cartcount = 0
    if current_user.is_authenticated:
        cartcount = Cart.query.filter_by(customerid=current_user.id).count()
    return cartcount


def removeitem(customerid, productid):
        #Use synchronize_session='fetch' so that once sqlalchemy deletes  a record on DB, removes it from query to avoid ObjectDeletedError
        #https://stackoverflow.com/questions/3481976/sqlalchemy-objectdeletederror-instance-class-at-has-been-deleted-help
        db.session.query(Cart).filter_by(customerid=customerid, productid=productid).delete(synchronize_session='fetch')
        db.session.commit()

def clearcart(customerid):
    Cart.query.filter_by(customerid=customerid).delete()
    db.session.commit()


def countcart(customerid):
    itemcount = Cart.query.filter_by(customerid=current_user.id).count()
    return itemcount


def getmpesacode(mpesacode):

    transaction = MpesaTransactions.query.filter_by(transactionid=mpesacode).first()

    if not transaction:
        response = {
        'message': 'Transaction has not yet been received.',
        'category': 'info',
        'amount': 0
        }
        return json.dumps(response)
    elif transaction.transactionstatus:
        response = {
        'message': 'Transaction has already been claimed',
        'category': 'warning',
        'amount': 0
        }
        return json.dumps(response)
    else:

        transaction.transactionstatus='claimed'
        db.session.commit()

        response = {
        'message': 'Payment added successfully',
        'category': 'success',
        'amount': transaction.transactionamount
        }
        return json.dumps(response)

def order_number_generator():
    ordernumber=''.join([random.choice(string.ascii_letters+string.digits) for n in range(10)]).upper()
    check_ordernumber=Orders.query.filter_by(orderno=ordernumber).first()

    if check_ordernumber:
        order_number_generator()

    return ordernumber


#Routes
@app.route('/order/product/', methods=['GET', 'POST'])
@login_required
def productdetails():
    cartcount = cart_count()
    product = Products.query.filter_by(id=request.form['productid']).first()

    return render_template('order/product.html', title="Order", cartcount=cartcount, product=product)


@app.route('/order/cart/', methods=['GET','POST'])
@login_required
def cart():
    totalprice = 0
    cartcount = cart_count()
    page = request.args.get('page', 1, type=int)

    cartall = Cart.query.filter_by(customerid=current_user.id).all()
    cart = Cart.query.filter_by(customerid=current_user.id).paginate(page=page, per_page=5)

    itemcount = Cart.query.filter_by(customerid=current_user.id).count()

    for item in cartall:
        product = Products.query.filter_by(id=item.productid).first()

        totalprice = totalprice + product.price


    if request.method=='POST':
        if request.form['btn_identifier'] == "removesingleitem":
            removeitem(current_user.id, request.form['productid'])
        else:
            clearcart(current_user.id)


    amountdue=totalprice

    form3 = PayNowForm()
    if form3.submit3.data and form3.validate():
        mpesacode = request.form['mpesacode'].upper()

        response_string = json.loads(getmpesacode(mpesacode))
        print(response_string)
        amountpaid = response_string['amount']
        print(amountpaid)
        flash(response_string['message'], response_string['category'])

        amountdue = totalprice - amountpaid


        # if amountdue <= 0:
        #     #add record to orders Table
        #     order = Orders(orderstatus='Pending', customerid=current_user.id, orderamount=totalprice)
        #     db.session.add(order)
        #     db.session.commit()
        #
        #     for item in cart:
        #         o = orders_products(orderid=order.id, productid=item.productid)
        #         db.session.add(o)
        #         db.session.commit()
        #
        #     #remove items from cart if payment completed
        #     for item in cart:
        #         db.session.delete(item)
        #     db.session.commit()


    return render_template('order/cart.html',
                            cartcount=cartcount,
                            title="Cart",
                            cart=cart,
                            form3=form3,
                            itemcount=itemcount,
                            totalprice=totalprice,
                            amountdue=amountdue
                            )


@app.route('/order/checkout', methods=['GET', 'POST'])
@login_required
def checkout():


    cartcount = cart_count()

    if request.method == "GET":

        pending_orders = Orders.query.filter_by(customerid=current_user.id, orderstatus='Pending').first()
        orderamount=pending_orders.orderamount
        amountdue = pending_orders.orderamount - pending_orders.amountpaid

    if request.method == "POST":

        cart=Cart.query.filter_by(customerid=current_user.id).all()
        orderamount=db.session.query(db.func.sum(Cart.price)).filter_by(customerid=current_user.id).scalar()

        orderno=order_number_generator()

        order = Orders(orderno=orderno, orderstatus='Pending', customerid=current_user.id, orderamount=orderamount)
        db.session.add(order)
        db.session.commit()

        for item in cart:
            record = orders_products(orderno=orderno, productid=item.productid)
            db.session.add(record)
            db.session.commit()

        order.itemcount=cartcount
        db.session.commit()


        clearcart(current_user.id)

        return redirect(url_for('checkout'))


    return render_template('order/checkout.html',
                            title="Cart",
                            cartcount=cartcount,
                            pending_orders=pending_orders,
                            amountdue=amountdue,
                            intasend_publishablekey = INTASEND_PUBLISHABLE_KEY,
                            intasend_redirecturl = INTASEND_SUCCESS_URL,
                            intasend_liveness = INTASEND_LIVENESS
                            )


@app.route('/order/receipt/')
def receipt():
    cartcount = cart_count()
    latestorder = Orders.query.filter_by(customerid=current_user.id).order_by(Orders.id.desc()).first()

    return render_template('order/receipt.html', title="Thank You", cartcount=cartcount, latestorder=latestorder)


@app.route('/order/failed/')
def failed_payment():
    cartcount = cart_count()
    latestorder = Orders.query.filter_by(customerid=current_user.id).order_by(Orders.id.desc()).first()

    return render_template('order/failed_payment.html', title="Payment Failed", cartcount=cartcount, latestorder=latestorder)



@app.route('/cart/add/', methods=['GET', 'POST'])
@login_required
def addtocart():

    if not current_user.is_authenticated:
        flash('Please login first to add items to your cart!', 'warning')
        return redirect(url_for('customerlogin'))

    if request.method == 'POST':

        productid = request.form['productid']

        product=Products.query.filter_by(id=productid).first()

        item = Cart(
                customerid=current_user.id,
                productid=productid,
                title=product.title,
                price=product.price,
                imagelocation=product.imagelocation
                )
        db.session.add(item)
        db.session.commit()

    return redirect(request.referrer)
