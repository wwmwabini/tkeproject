import requests
import random
import string
from flask import request,json
from tkeproject import app, db
from tkeproject.models import MpesaTransactions
from requests.auth import HTTPBasicAuth
from config import CONSUMER_KEY, CONSUMER_SECRET, MPESA_TOKEN_ENDPOINT, MPESA_REGISTERURLS_ENDPOINT, MPESA_SIMULATION_ENDPOINT, BASE_URL



def accesstoken():
    data = (requests.get(MPESA_TOKEN_ENDPOINT, auth=HTTPBasicAuth(CONSUMER_KEY, CONSUMER_SECRET))).json()
    return data['access_token']


@app.route('/c2b/accesstoken')
def test_accesstoken():
    data = accesstoken()
    return data


@app.route('/c2b/registerurls')
def registerurls():
    headers = {
    "Authorization": "Bearer %s" %accesstoken()
    }

    payload = {
        "ShortCode": '600997',
        "ResponseType": 'Completed',
        "ConfirmationURL": BASE_URL + "/c2b/confirmation/",
        "ValidationURL": BASE_URL + "/c2b/validation/"
    }

    print(payload['ConfirmationURL'])
    print(payload['ValidationURL'])

    responsedata = requests.post(
        MPESA_REGISTERURLS_ENDPOINT,
        json = payload,
        headers = headers
    )

    return responsedata.json()



@app.route('/c2b/confirmation/', methods=['POST'])
def confirmation():

    confirmation_data = request.get_json()
    # confirmation_data = {
    #    "TransactionType":"PayBill",
    #    "TransID":"RKTQDM7W6S",
    #    "TransTime":"20191122063845",
    #    "TransAmount":"150",
    #    "BusinessShortCode":"600638",
    #    "BillRefNumber":"254708374149",
    #    "InvoiceNumber":"",
    #    "OrgAccountBalance":"49197.00",
    #    "ThirdPartyTransID":"",
    #    "MSISDN":"254708374149",
    #    "FirstName":"John",
    #    "MiddleName":"",
    #    "LastName":"Doe"
    # }
    #
    #
    # fake_transactionid = ''.join(random.choice(string.ascii_uppercase+string.digits) for j in range(10))
    #
    # transaction = MpesaTransactions(
    #                     transactiontype = confirmation_data['TransactionType'],
    #                     transactionid = fake_transactionid,
    #                     transactiontime =  confirmation_data['TransTime'],
    #                     transactionamount = confirmation_data['TransAmount'],
    #                     business_shortcode = confirmation_data['BusinessShortCode'],
    #                     msisdn = confirmation_data['MSISDN'],
    #                     firstname = confirmation_data['FirstName'],
    #                     lastname = confirmation_data['LastName']
    #                 )
    # db.session.add(transaction)
    # db.session.commit()

    #write to log as well
    file = open('confirm.json', 'a')
    file.write(json.dumps(confirmation_data))
    file.close()

    return {
      "ResultCode": 0,
      "ResultDesc": "Accepted"
    }



@app.route('/c2b/validation/', methods=['POST'])
def validation():
    validation_data = request.get_data()

    file = open('validate.json', 'a')
    file.write(validation_data)
    file.close()


    return {
      "ResultCode": 0,
      "ResultDesc": "Accepted"
    }



@app.route('/c2b/simulate')
def simulate():

    access_token = accesstoken()

    headers = {
    "Authorization": "Bearer %s" % access_token
    }

    payload = {
       "CommandID": "CustomerBuyGoodsOnline",
       "Amount":"5",
       "Msisdn":"254705912645",
       "BillRefNumber":"",
       "ShortCode":"600977"
    }

    responsedata = requests.post(
        MPESA_SIMULATION_ENDPOINT,
        json = payload,
        headers = headers
    )

    return responsedata.json()
