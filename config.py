import os
from dotenv import load_dotenv

load_dotenv()

#app secret
APP_SECRET_KEY = os.environ.get('APP_SECRET_KEY')

#mysql settings
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')
DB_USER = os.environ.get('DB_USER')
DB_PASS = os.environ.get('DB_PASS')
DB_NAME = os.environ.get('DB_NAME')

DB_CONNECTION_STRING = os.environ.get('DB_CONNECTION_STRING')

#mail settings
MAIL_SERVER = os.environ.get('MAIL_SERVER')
MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
MAIL_PORT = os.environ.get('MAIL_PORT')
MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
MAIL_DEBUG = os.environ.get('MAIL_DEBUG')

#mpesa
CONSUMER_KEY = os.environ.get('CONSUMER_KEY')
CONSUMER_SECRET = os.environ.get('CONSUMER_SECRET')
MPESA_TOKEN_ENDPOINT = os.environ.get('MPESA_TOKEN_ENDPOINT')
MPESA_REGISTERURLS_ENDPOINT = os.environ.get('MPESA_REGISTERURLS_ENDPOINT')
MPESA_SIMULATION_ENDPOINT = os.environ.get('MPESA_SIMULATION_ENDPOINT')
BASE_URL = os.environ.get('BASE_URL')

#jisortsms
JISORT_SMS_USERNAME=os.environ.get('JISORT_SMS_USERNAME')
JISORT_SMS_PASSWORD=os.environ.get('JISORT_SMS_PASSWORD')

#recaptcha
RECAPTCHA_PUBLIC_KEY=os.environ.get('RECAPTCHA_PUBLIC_KEY')
RECAPTCHA_PRIVATE_KEY=os.environ.get('RECAPTCHA_PRIVATE_KEY')

#intasend
INTASEND_PUBLISHABLE_KEY=os.environ.get('INTASEND_PUBLISHABLE_KEY')
INTASEND_SUCCESS_URL=os.environ.get('INTASEND_SUCCESS_URL')
INTASEND_CHALLENGE=os.environ.get('INTASEND_CHALLENGE')
INTASEND_LIVENESS=os.environ.get('INTASEND_LIVENESS')
